﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

[System.Serializable]
public class AxleInfo //info de los ejes y sus respectivos componentes
{
    //colliders de las ruedas
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    
    public bool motor; //bool de si esta rueda está conectada al motor
    public bool brakes; //bool de si esta rueda tiene freno
    public bool steering; //bool de si esta rueda gira
}

public class CarController : MonoBehaviour //carcontroller
{
    private Rigidbody rb; //rigibdoby que uso para calcular la velocidad
    public CocheData carData; //SO con la info del coche

    public List<AxleInfo> axleInfos; //lista de los ejes del coche
    public float maxMotorTorque; //torque máximo del motor del coche
    public float maxSteeringAngle; //ángulo de giro máximo del coche
    public float brakeTorque; //torque actual de los frenos del coche

    void Awake()
    {
        rb = GetComponent<Rigidbody>(); //asigno a rb el rigibdoby del coche
    }

    void FixedUpdate()
    {
        //currentVelocity = rb.velocity; 
        carData.actualSpeed = (float)Math.Round(rb.velocity.magnitude * 3.6f); //velocidad actual en m/s lo paso a km/h
        //Si la velocidad alcanzada es superior a la máxima previamente guardada, se sobreescribe
        if (carData.actualSpeed > carData.maxSpeedAchieved)
            carData.maxSpeedAchieved = carData.actualSpeed;

        /*Input para ya sea con el stick izquierdo de un mando 
         * en el eje Y o con teclado W/S acelerar y frenar */
        //float motor = maxMotorTorque * CrossPlatformInputManager.GetAxis("Vertical");
        //Input para acelerar y frenar con mando de Xbox One(A aceleras y X frenas/marcha atrás)
        float motor = maxMotorTorque * CrossPlatformInputManager.GetAxis("A");

        /*Input para ya sea con el stick izquierdo de un mando 
         * en el eje X o con teclado A/D girar izquierda/derecha */
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

        //Input para frenar con mando de Xbox One(B)
        //float brake = brakeTorque * CrossPlatformInputManager.GetAxis("B");

        //Input para reiniciar escena con mando de Xbox One(Y)
        if (Input.GetButton("Y"))
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
        //Input para derrapar con mando de Xbox One(B)
        if (Input.GetButton("B"))
        {   //le subo el slip de la fricción lateral a las ruedas con motor, así puede derrapar
            foreach (AxleInfo axleInfo in axleInfos)
            {
                if (axleInfo.motor)
                {
                    WheelFrictionCurve wfc = new WheelFrictionCurve(); 
                    wfc.extremumSlip = 1f; 
                    wfc.extremumValue = 1;
                    wfc.asymptoteSlip = 0.5f;
                    wfc.asymptoteValue = 0.75f;
                    wfc.stiffness = 1;
                    axleInfo.rightWheel.sidewaysFriction = wfc;
                    axleInfo.leftWheel.sidewaysFriction = wfc;
                }
            }
        }
        else
        {
            foreach (AxleInfo axleInfo in axleInfos)
            {               
                WheelFrictionCurve wfc = new WheelFrictionCurve();
                wfc.extremumSlip = 0.2f;
                wfc.extremumValue = 1;
                wfc.asymptoteSlip = 0.5f;
                wfc.asymptoteValue = 0.75f;
                wfc.stiffness = 1;
                axleInfo.rightWheel.sidewaysFriction = wfc;
                axleInfo.leftWheel.sidewaysFriction = wfc;                
            }
        }

        //por cada eje, si el bool es true, le aplica lo siguiente 
        foreach (AxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.steering) //puede girar
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor) //recibe fuerza del motor
            {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
            }
            /*if (axleInfo.brakes) //puede frenar
            {
                axleInfo.leftWheel.brakeTorque = brake;
                axleInfo.rightWheel.brakeTorque = brake;
            }*/
        }
    }
}
