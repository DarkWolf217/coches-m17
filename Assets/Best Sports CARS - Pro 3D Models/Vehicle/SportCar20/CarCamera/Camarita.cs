﻿using UnityEngine;

public class Camarita : MonoBehaviour
{
	public Transform cochePos; //transform del coche
	public Vector3 offset; //offset de la cámara
	public float segSpeed; //velocidad de seguimiento(si es baja, más se la aleja del coche cuando aceleras)

	private void FixedUpdate()
	{
		//posición de la cámara
		Vector3 posCam = cochePos.position + cochePos.forward*offset.z + 
			cochePos.right*offset.x + cochePos.up*offset.y;
		transform.position = Vector3.Lerp(transform.position, posCam, segSpeed * Time.deltaTime);

		//dirección de la cámara
		Vector3 dirCam = new Vector3(cochePos.transform.position.x, cochePos.transform.position.y+2,
			cochePos.transform.position.z);
		transform.LookAt(dirCam);
	}
}