﻿using TMPro;
using UnityEngine;

public class HUD : MonoBehaviour
{
    public TMP_Text maxVel; //el texto donde se muestra la velocidad máxima alcanzada
    public TMP_Text actVel; //el texto donde se muestra la velocidad actual
    public CocheData carData; //SO con la info del coche

    //actualizo el HUD
    void Update() 
    {
        maxVel.text = "Velocidad máxima alcanzada: " + carData.maxSpeedAchieved.ToString();
        actVel.text = carData.actualSpeed.ToString() + " hm/h";
    }
}


