﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Car Data")]
public class CocheData : ScriptableObject //SO para guardar info del coche
{
    public float maxSpeedAchieved;
    public float actualSpeed;
}
